import socket
import time

IP_Address_R9_PC = '127.0.0.1'
TCP_Port_R9s = 12600

BUFFER_SIZE = 1024

def send(command):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IP_Address_R9_PC, TCP_Port_R9s))

    # KEEP THE WAIT TIME BELOW, otherwise the R9 will ignore your first command
    time.sleep(0.1)

    #print ("Connected to socket \n")
    #command = "GetSWSubItemParameter, Scan Area Window, MeasureSave, Default Comment"
    command += "\n"

    s.send(command.encode())
    #print ("Command sent \n")

    output = s.recv(BUFFER_SIZE)
    #print ("Output received \n")
    s.close()

    #print ("Received output:", output)
    return output.decode()
