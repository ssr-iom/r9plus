import time
from pynput import mouse
import win32gui
from r9tools import send

class MouseClick():
    '''Class representing a mouse click
    '''

    def __init__(self):

        absX = 0
        absY = 0
        mouseButton = 0

        def on_click(x, y, button, pressed):
            nonlocal absX
            nonlocal absY
            nonlocal mouseButton
            absX = x
            absY = y
            mouseButton = button
            return pressed

        with mouse.Listener(on_move=None, on_click=on_click, on_scroll=None) as listener:
            listener.join()

        self.time = time.time()

        self.absX = absX
        self.absY = absY
        self.button = mouseButton
        self.isleft = False
        self.ismiddle = False
        self.isright = False
        if self.button == mouse.Button.left:
            self.isleft = True
        elif self.button == mouse.Button.middle:
            self.ismiddle = True
        elif self.button == mouse.Button.right:
            self.isright = True

    def is_in(self, rect):
        x0 = rect[0]
        y0 = rect[1]
        x1 = rect[2]
        y1 = rect[3]
        if x0 <= self.absX <= x1 and y0 <= self.absY <= y1:
            return True
        else:
            return False

    def rel_pos_in(self, rect):
        rel = [-1, -1]
        if self.is_in(rect):
            rel[0] = rect[0] - self.absX
            rel[1] = rect[1] - self.absY
        return rel

class TopoClick(MouseClick):
    '''Class representing a mouse click in a Topography window
    '''

    def __init__(self):

        MouseClick.__init__(self)

        topoFwd = TopoWin("Topography Forward ")
        topoRev = TopoWin("Topography Reverse ")

        self.intopo = True

        if self.is_in(topoFwd.rect):
            self.topo = topoFwd
            self.nmX, self.nmY = self.nm_pos_in(topoFwd)
        elif self.is_in(topoRev.rect):
            self.topo = topoRev
            self.nmX, self.nmY = self.nm_pos_in(topoRev)
        else:
            self.fwd = None
            self.intopo = False
            self.nmX = None
            self.nmY = None

    def nm_pos_in(self, topo):
        nmSize = float(send("GetSWParameter, Scan Area Window, Scan Area Size"))
        nmX = -(self.rel_pos_in(topo.rect)[0]/topo.frameSize + 0.5) * nmSize
        nmY = (self.rel_pos_in(topo.rect)[1]/topo.frameSize + 0.5) * nmSize
        return nmX, nmY

class R9Win():
    '''Class representing a window in R9S software
    '''

    def __init__(self, title):
        self.title = title
        rect = self.winRect()
        if rect:
            self.exists = True
            self.rect = rect
        else:
            self.exists = False
            self.rect = [-1, -1, -1, -1]

    def winRect(self):
        '''Return a list of window coordinates based on its title'''
        def callback(hwnd, rect):
            if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
                winTitle = win32gui.GetWindowText(hwnd)
                if winTitle.startswith(self.title):
                    winRect = win32gui.GetWindowRect(hwnd)
                    rect.append(winRect)
            return True
        rect = []
        win32gui.EnumWindows(callback, rect)
        return list(rect[0])

class TopoWin(R9Win):
    '''Class representing a Topography window in R9S software
    '''

    def __init__(self, title):

        R9Win.__init__(self, title)

        # Topography frame (square) size is calculated along
        # y direction, accounting also topography window borders
        self.rect[0] += 10
        self.rect[1] += 57
        self.rect[2] -= 23
        self.rect[3] -= 30
        self.frameSize = self.rect[3] - self.rect[1]
