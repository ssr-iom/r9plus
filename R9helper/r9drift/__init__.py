import time
from r9tools import send

def set_drift(click1, click2):
    '''Calculate drift and send it to R9S
    '''

    new_dx = (click2.nmX - click1.nmX) / (click2.time - click1.time)
    new_dy = (click2.nmY - click1.nmY) / (click2.time - click1.time)
    old_dx = float(send("GetSWSubItemParameter, Scan Area Window, Drift Settings, Vector dx"))
    old_dy = float(send("GetSWSubItemParameter, Scan Area Window, Drift Settings, Vector dy"))

    vector_dx = old_dx + new_dx
    vector_dy = old_dy + new_dy

    if abs(vector_dx) < 1E-8 and abs(vector_dy) < 1E-8:
        send("SetSWSubItemParameter, Scan Area Window, Drift Settings, Vector dx, {}".format(vector_dx))
        send("SetSWSubItemParameter, Scan Area Window, Drift Settings, Vector dy, {}".format(vector_dy))
        print("Drift vectors updating. Please wait...")
        update_drift()
        print("Drift applied.")
    else:
        print("Drift vectors out of range. Aborted")
    return 0

def update_drift():
    '''Update drift values in R9S
    '''

    # Get current scan parameters
    lines = int(send("GetSWSubItemParameter, Scan Area Window, Scan Settings, Lines Per Frame"))
    size = float(send("GetSWParameter, Scan Area Window, Scan Area Size"))
    speed = float(send("GetSWParameter, Scan Area Window, Scan Speed"))

    # Stop scan and set fast scan parameters
    send("StopProcedure, Comb Scan 2")
    send("SetSWSubItemParameter, Scan Area Window, Scan Settings, Lines Per Frame, 8")
    send("SetSWParameter, Scan Area Window, Scan Area Size, 2e-09")
    send("SetSWParameter, Scan Area Window, Scan Speed, 3e-07")

    # Start and stop a scan
    send("StartProcedure, Comb Scan 2")
    time.sleep(1)
    send("StopProcedure, Comb Scan 2")

    # Set previous scan parameters and restart scan
    send("SetSWSubItemParameter, Scan Area Window, Scan Settings, Lines Per Frame, {}".format(lines))
    send("SetSWParameter, Scan Area Window, Scan Area Size, {}".format(size))
    send("SetSWParameter, Scan Area Window, Scan Speed, {}".format(speed))

    return 0
