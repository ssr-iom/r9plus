#!C:\Users\spmuser\Miniconda3\envs\r9plus\python.exe
# -*- coding: iso-8859-1 -*-

import time
from r9gui import TopoClick
from r9drift import set_drift

def point_drift():
    '''Calculate point drift.
    '''

    print("POINT DRIFT CORRECTION.")

    # Wait for 1st click
    print(" > Left click to set reference point.")
    click1 = TopoClick()

    # Check 1st click
    if click1.isleft and click1.intopo:
        print("Reference point set: ", click1.nmX, click1.nmY)

        # Wait for 2nd click
        print(" > Left click to apply drift. Right click to abort.")
        second_click(click1)

    else:
        print("Drift correction aborted.")
        time.sleep(2)
        return -1

def second_click(click1):

    click2 = TopoClick()

    # Check 2nd click
    if click2.is_in(click1.topo.rect):
        if click2.isright:
            print("Drift correction aborted.")
            time.sleep(2)
            return -1
        if click2.isleft:
            print("Current point set: ", click2.nmX, click2.nmY)
            set_drift(click1, click2)
            time.sleep(2)
            return 0
    else:
        second_click(click1)

if __name__ == '__main__':
    point_drift()
