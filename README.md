The script requires pynput and pywin32 Python packages.

conda install pip
pip install pynput
conda install -c conda-forge pywin32

It is called with "python.exe point_drift.py"